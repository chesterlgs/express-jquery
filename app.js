var express=require('express');
var app=express();
var path=require('path');
var bodyParser=require('body-parser');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));

app.set('view engine','ejs');
app.set('views',path.join(__dirname,'/views'))

app.use(express.static(path.join(__dirname,'public')));

app.use('/',require('./router/index'));

app.listen(3000);